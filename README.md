<div align="center">

<p align="center">
  <a href="" rel="noopener">
    <img width="30%" src="./ansible-wordmark.png" alt="Ansible Wordmark"></a>
  </a>
</p>

  [![Status](https://img.shields.io/badge/status-Active-success.svg)]() 
  [![Ansible Galaxy](https://img.shields.io/ansible/role/ROLEID)]() 
  [![Ansible Version](https://img.shields.io/badge/ansible-2.10-informational)]() 
  [![Ansible Version](https://img.shields.io/ansible/quality/TODO)]() 
  [![License](https://img.shields.io/badge/license-GPLV3-blue.svg)](/LICENSE)

</div>

---

# Ansible - System Avahi Alias

Sets up avahi so that a systemd service can be used to broadcast multiple ZeroConf aliases.

- Installs Avahi Utils
- Installs the Systemd Service
- Takes a list of aliases and enables a service for each one of them.

## 🦺 Requirements

*None*

## 🗃️ Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

```yaml
    # Aliases. No need to add the .local
    sys_avahi_aliases:
      - nas
      - server
      - rpi4
```

## 📦 Dependencies

*None*

## 🤹 Example Playbook

*TODO*

## ⚖️ License

This code is released under the [GPLV3](./LICENSE) license. For more information refer to `LICENSE.md`

## ✍️ Author Information

- Original idea from [nonrectangular](https://serverfault.com/users/542803/nonrectangular) on [this threat](https://serverfault.com/questions/268401/configure-zeroconf-to-broadcast-multiple-names/986437).
- Adapted as an Ansible role by <Gerson Rojas>
